-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 31, 2016 at 08:54 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `catagory`
--
CREATE DATABASE IF NOT EXISTS `catagory` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `catagory`;

-- --------------------------------------------------------

--
-- Table structure for table `distric`
--

CREATE TABLE `distric` (
  `id` int(15) NOT NULL,
  `distric_name` varchar(255) NOT NULL,
  `created` date DEFAULT NULL,
  `updated` date DEFAULT NULL,
  `deleted` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `distric`
--

INSERT INTO `distric` (`id`, `distric_name`, `created`, `updated`, `deleted`) VALUES
(1, 'Dhaka', NULL, NULL, NULL),
(2, 'Comilla', NULL, NULL, NULL),
(3, 'Noakhali', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sub_distric`
--

CREATE TABLE `sub_distric` (
  `id` int(11) NOT NULL,
  `sub_dis_name` varchar(255) NOT NULL,
  `dist_id` varchar(255) NOT NULL,
  `created` date DEFAULT NULL,
  `updated` date DEFAULT NULL,
  `deleted` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_distric`
--

INSERT INTO `sub_distric` (`id`, `sub_dis_name`, `dist_id`, `created`, `updated`, `deleted`) VALUES
(1, 'Demra', '1', NULL, NULL, NULL),
(2, 'Kadamtali', '1', NULL, NULL, NULL),
(3, 'Mirpur', '1', NULL, NULL, NULL),
(4, 'Comilla Kotbari', '2', NULL, NULL, NULL),
(5, 'Tomsom Bridge', '2', NULL, NULL, NULL),
(6, 'Sonaimuri', '3', NULL, NULL, NULL),
(7, 'Sonapur', '3', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `village`
--

CREATE TABLE `village` (
  `id` int(15) NOT NULL,
  `village_name` varchar(255) NOT NULL,
  `sub_dis_id` varchar(255) NOT NULL,
  `created` date DEFAULT NULL,
  `updated` date DEFAULT NULL,
  `deleted` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `village`
--

INSERT INTO `village` (`id`, `village_name`, `sub_dis_id`, `created`, `updated`, `deleted`) VALUES
(1, 'Rayerbag', '1', NULL, NULL, NULL),
(2, 'kalikapur', '6', NULL, NULL, NULL),
(3, 'Rajarampur', '6', NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `distric`
--
ALTER TABLE `distric`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_distric`
--
ALTER TABLE `sub_distric`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `village`
--
ALTER TABLE `village`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `distric`
--
ALTER TABLE `distric`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `sub_distric`
--
ALTER TABLE `sub_distric`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `village`
--
ALTER TABLE `village`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
